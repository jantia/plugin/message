<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Message
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Plugin\Message\Exception;

/**
 *
 */
class RuntimeException extends \RuntimeException implements ExceptionInterface {

}
